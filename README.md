# Python Skript Boilerplate

This python boilerplate script illustrates how to parallelize CPU-bound tasks with a process pool by using a contrived image processing application as an example.

By default the pool is initialized with as many worker processes as available CPUs provided by the hardware. The number of worker processes can be overriden. The `compute_heart_valve_surface_from_image` function is executed by the process workers and contains the CPU-intensive code. Additionally the input (image paths in this example) can be chunked. This helps to control the memory load for huge amount of inputs, large computation results or both. By splitting the inputs into chunks the process pool can be feed with one chunk at a time instead of all inputs at once. The `handle_chunk_results` function allows to handle the results of each chunk, e.g. persisting them to a file or a database.

## Install

1. Verify that python3 is installed:

   ```bash
   $ python --version
   Python 3.x.y
   ```

2. Clone the repository: 

   ```bash
   $ git clone https://gitlab.com/tooreht/python-script-boilerplate.git
   cd python-script-boilerplate
   ```

3. Create a virtualenv directory called venv:

   ```bash
   $ python -m venv venv
   ```

4. Activate your virtualenv:

   ```bash
   $ . ./venv/bin/activate
   ```
5. Install requirements via pip:

   ```bash
   $ pip install -r requirements.txt
   ```

## Usage

   ```bash
   $ ./script.py --help
   usage: script.py [-h] [-p POOL_SIZE] [-c CHUNK_SIZE] [IMG [IMG ...]]
   
   Compute surfaces of heart valve images.
   
   positional arguments:
	 IMG                   Heart valve image paths.
   
   optional arguments:
	 -h, --help            show this help message and exit
	 -p POOL_SIZE, --pool-size POOL_SIZE
						   Number of processes in the worker pool
	 -c CHUNK_SIZE, --chunk-size CHUNK_SIZE
						   Number of images that are processed at each iteration
   ```

## Examples

Process all files located in the image directory which end with the suffix `.jpg`

   ```bash
   $ ./script.py images/*.jpg
   ```

Additionally split the files in chunks of 1000 images (to control memory size of computations)

   ```bash
   $ ./script.py images/*.jpg --chunk-size 1000
   ```

Additionally limit pool size to 2 process workers for consuming the chunks (defaults to available cpu cores,
so normally doesn't need to be specified for optimal performance)

   ```bash
   $ ./script.py images/*.jpg --chunk-size 1000 --pool-size 2
   ```

To demonstrate performance gains run

   ```bash
   $ ./script.py images/*.jpg --chunk-size 1000 --pool-size 1
   ```

to simulate running with only 1 CPU core and then


   ```bash
   $ ./script.py images/*.jpg --chunk-size 1000
   ```

to use all available CPU cores. This should be roughly n times faster, where n = number of CPU cores.

## Tipps

To persist exact versions of all installed libraries use

   ```bash
   $ pip freeze > requirements.txt
   ```
