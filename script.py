#!/usr/bin/env python

import argparse
import csv
import datetime
import multiprocessing
import os
import random
import time


RESULT_DIR = 'results'


def compute_heart_valve_surface_from_image(image_path):
	"""
	Compute heart valve surface size from image.

	!!!Dummy implementation with random values, replace this function with your real world code!!!
	"""
	# simulate CPU load
	seconds = random.randint(0, 4)  # randomly wait 0 to 3 seconds
	time.sleep(seconds)

	# generate random results
	surface = random.randrange(0, 101)  # randomly generate results from 0 to 100
	return image_path, surface

def handle_chunk_results(execution, chunk_id, results):
	# Persist results per chunk as csv
	file_name = f'{execution}_chunk{chunk_id}.csv'
	path = os.path.join(RESULT_DIR, file_name)
	with open(path, 'w', newline='') as csv_file:
		writer = csv.writer(csv_file, delimiter=',',
							quotechar='|', quoting=csv.QUOTE_MINIMAL)
		writer.writerow(['image', 'surface'])
		writer.writerows(results)

def chunkify(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in range(0, len(l), n):
		yield l[i:i + n]

def main(args):
	"""Main function which orchestrates the processing flow."""
	start = time.time()

	# Start a process pool of desired size
	with multiprocessing.Pool(processes=args.pool_size) as pool:
		execution = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

		# Generate chunks
		if args.chunk_size == 0:  # means using no chunks
			chunks = [args.images]
		else:
			chunks = chunkify(args.images, args.chunk_size)

		# Go through all chunks
		for idx, chunk in enumerate(chunks):
			# Feed process pool with one chunk per iteration and wait for the computation results.
			# This distributes all images of the current chunk to the process pool workers.
			# All workers run the compute_heart_valve_surface_from_image function concurrently.
			results = pool.map(compute_heart_valve_surface_from_image, chunk)
			handle_chunk_results(execution, idx, results)

	end = time.time()
	print(f"Elapsed time: {end - start}s")



if __name__ == '__main__':
	#
	# COMMAND LINE PARSING
	#
	parser = argparse.ArgumentParser(description='Compute surfaces of heart valve images.')
	parser.add_argument('images',
						metavar='IMG',
						type=str,
						nargs='*',
						default=[],
						help='Heart valve image paths.')
	parser.add_argument('-p', '--pool-size',
						type=int,
						default=None,  # processes=None means to use as many processes as available cpu cores
						help='Number of processes in the worker pool')
	parser.add_argument('-c', '--chunk-size',
						type=int,
						default=0,
						help='Number of images that are processed at each iteration')
	args = parser.parse_args()



	#
	# MAIN
	#
	main(args)
